#!/bin/bash

source .env

THEMES_DIR="wp-content/themes"
GOF_NAME=$(basename "$GOF_REPO" ".${GOF_REPO##*.}")
GOF_DIR=${THEMES_DIR}/${GOF_NAME}
if [[ ! -z ${CUSTOMER_REPO} ]]; then
  CHILD_NAME=$(basename "$CUSTOMER_REPO" ".${CUSTOMER_REPO##*.}")
  CHILD_DIR=${THEMES_DIR}/${CHILD_NAME}
fi

# Print info on error usage
error_usage() {
 echo 'Choose an argument:
  -p | --pot to generate a pot file
  -j | --json to generate a json file' >&2
  exit 1
}

# Export POT file
generate_pot() {
	echo "WP_Docker - Preparing the .pot files."
  sudo chown -R www-data:www-data wp-app/${GOF_DIR}/languages/
	if [[ ! -z ${CHILD_DIR} ]]; then
		echo "WP_Docker - Existing child theme. Generation of its pot files too."
    sudo chown -R www-data:www-data wp-app/${CHILD_DIR}/languages/
		docker-compose run --rm wpcli sh -c "wp i18n make-pot ${GOF_DIR} ${GOF_DIR}/languages/${GOF_NAME}.pot && \
											                   wp i18n make-pot ${CHILD_DIR} ${CHILD_DIR}/languages/${CHILD_NAME}.pot"
    sudo chown -R $USER:$USER wp-app/${CHILD_DIR}/languages/
	else
		docker-compose run --rm wpcli sh -c "wp i18n make-pot ${GOF_DIR} ${GOF_DIR}/languages/${GOF_NAME}.pot"
	fi
  sudo chown -R $USER:$USER wp-app/${GOF_DIR}/languages/
}

# Export JSON file
generate_json() {
	echo "WP_Docker - Preparing the .json files."
  if [[ ! -f wp-app/${GOF_DIR}/languages/${GOF_NAME}.pot ]]; then
      generate_pot
  fi
	if [[ ! -z ${CHILD_DIR} ]]; then
		echo "WP_Docker - Existing child theme. Generation of its json files too."
		docker-compose run --rm wpcli sh -c "wp i18n make-json ${GOF_DIR}/languages/ --no-purge --pretty-print && \
											                   wp i18n make-json ${CHILD_DIR}/languages/ --no-purge --pretty-print"
	else
		docker-compose run --rm wpcli sh -c "wp i18n make-json ${GOF_DIR}/languages/ --no-purge --pretty-print"
	fi
}

# Loop through arguments and process them
for arg in "$@"
do
    case $arg in
        -p|--pot)
          generate_pot
          ;;
        -j|--json)
          generate_json
          ;;
        *)
          error_usage
          ;;
    esac
done

exit 0
