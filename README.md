# WP Docker

Contains a Docker instance to power WordPress.  
Before start docker services for the first time, be aware to edit .env file from .env.sample.  
Many variables are used in another scripts.  

## Install docker containers

### Windows

Install Docker Desktop, Docker Compose and WSL : <https://docs.docker.com/docker-for-windows/wsl/>

### Linux et Mac

Note: not tested on MAC.  
Install Docker Compose : <https://docs.docker.com/compose/install/#install-compose>

---

## Edit .env file from .env.sample

PROJECT=project_name  
CUSTOMER=customer_name  
PROD=example.com  
STAGING=dev.example.com  
DB_USERNAME=what_you_want  
DB_ROOT_PASSWORD=what_you_want  
DB_NAME=what_you_want  
WP_VERSION=latest_or_what_you_want  
WP_ADMIN_USER=what_you_want  
WP_ADMIN_PASSWORD=what_you_want  
GOF_REPO=git@gitlab.com:kaeles/gear-of-web.git  
CUSTOMER_REPO=git@example.com:user/what_you_want  
USER_ID=33  
GROUP_ID=33  

## Start Docker services

`docker-compose up -d`  
Containers created :

- wordpress_${CUSTOMER} -> HTTP server, call localhost
- pma_${CUSTOMER} -> phpMyAdmin instance, call localhost:8080
- db_${CUSTOMER} -> MySQL database
- wpcli_${CUSTOMER} -> WP_CLI service

---

## INIT WordPress

`./wp-init.sh` init themes using .env informations.

## NPM management

### NPM dependencies install

`cd ${ROOT_POJECT} && npm install`

Some NPM scripts are available from ${ROOT_POJECT} path.  

Scripts name for Gear of Web theme.  

- `npm run build-gof` GOF WordPress theme build.
- `npm run start-gof` GOF WordPress theme watch.

Note: use these after update or editing this theme.  

Scripts name for customer theme.  

- `npm run build` Customer WordPress theme build.
- `npm run start` Customer WordPress theme watch.

---

## Database

To import database, Go to phpMyAdmin [http://localhost:8080] then import database from staging or production environment.  
Note: Please, change URL presets on your db file.

To export database, use:  
`./export.sh $_environement` where $_environement is prod or staging.  
Note: This script exports the database replacing the site URLs depending on the destination environment.

---

## Internationalization

To create pot file, use:  
`./translate.sh -p` or `./translate.sh --pot`  
Note: if child theme exists, this script will automatically generate his pot file.
