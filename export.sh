#!/bin/bash
source .env
_now=$(date +"%m_%d_%Y")
_file="data_${_now}.sql"
_dn="localhost"

for arg in $@; do
    _environment=$arg
done

if [[ -n $_environment ]]; then
    _file="${_environment}_${_file}"
fi

if [[ $_environment == "prod" ]]; then
    _dn=$PROD
else [[ $_environement == "staging" ]]
    _dn=$STAGING
fi

if [[ ! -d "wp-data" ]]; then
    mkdir wp-data
fi

# Export dump
EXPORT_PATH="/var/www/wp-data/${_file}"
docker-compose run --rm wpcli sh -c "wp search-replace localhost ${_dn} --export=${EXPORT_PATH}"
sleep 15
exit 0
