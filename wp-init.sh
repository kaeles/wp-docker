#!/bin/bash
# This script will get WordPress themes from Rolling Web repositories.
# Use this script only when creating the project.

if [[ -f wp-init.lock ]]; then
  echo "WP_Docker - Initialization already done."
  sleep 5
  exit 1
fi

# get Gear of Web basename directory from GOF_REPO
source .env
ROOT_DIR=$(pwd)
GOF_DIR=$(basename "$GOF_REPO" ".${GOF_REPO##*.}")
THEMES_DIR="${ROOT_DIR}/wp-app/wp-content/themes/"
PROJECT_DIR=$(basename "$CUSTOMER_REPO" ".${CUSTOMER_REPO##*.}")

INSTALL_COMMAND="--url=localhost \
                    --title=$PROJECT \
                    --admin_user=$WP_ADMIN_USER \
                    --admin_password=$WP_ADMIN_PASSWORD \
                    --admin_email=$(git config user.email) \
                    --skip-email"
PLUGIN_LIST="debug-bar cookie-notice regenerate-thumbnails wp-sweep wpforms-lite"

## MAIN SCRIPT
sudo chown -R $USER:$USER $THEMES_DIR # current user have to work with this directory
docker-compose rm -f wpcli # prevent containers duplication
cd $THEMES_DIR

# Clean theme directory
echo "WP_Docker - Clean theme directory."
sleep 2
find . -name "twenty*" -type d -exec rm -rf {} +

# Prepare themes
echo "WP_Docker - Import Gear of Web and if needed the customer theme."
sleep 2
# Get Child theme if neceserry
if [[ ${CUSTOMER_REPO} ]]; then
  git clone ${CUSTOMER_REPO}
  mkdir "${PROJECT_DIR}/languages"
  chown -R www-data:www-data "${PROJECT_DIR}/languages"
fi
# git clone Gear Of Web Theme
git clone $GOF_REPO
chown -R $USER:$USER ${PROJECT_DIR}
chown -R www-data:www-data "${PROJECT_DIR}/languages"
cd $GOF_DIR
echo "WP_Docker - Install GOF dependencies."
npm install --silent

# Install main dev dependecies to installed into root project.
echo "WP_Docker - Install webpack dependencies."
sleep 2
cd $ROOT_DIR
npm install --silent

# Build GOF theme
echo "WP_Docker - Build GOF theme."
sleep 2
npm run --silence build-gof

# Install WP Database, WP Plugins Dev and if nedeed the child theme
echo "WP_Docker - Proceed WordPress initialization."
if [[ $PROJECT_DIR ]]; then
  CHILD_CONFIG="$PROJECT_DIR \
                --parent_theme=$GOF_DIR \
                --theme_name=$PROJECT \
                --author='$(git config user.name)'"
  docker-compose run --rm wpcli sh -c "wp core install $INSTALL_COMMAND && \
                                        wp language core install fr_FR && \
                                        wp site switch-language fr_FR && \
                                        wp plugin install $PLUGIN_LIST --activate && \
                                        wp scaffold child-theme $CHILD_CONFIG"
else
  docker-compose run --rm wpcli sh -c "wp core install $INSTALL_COMMAND && \
                                        wp language core install fr_FR && \
                                        wp site switch-language fr_FR && \
                                        wp plugin install $PLUGIN_LIST --activate"
fi

echo "WP_Docker - Initialization has proceed."
touch ${ROOT_DIR}/wp-init.lock
sleep 5
exit 0
